package io.javabrains.repuestos.rest;


import io.javabrains.repuestos.models.User;
import io.javabrains.repuestos.models.UserTokenState;

public class AuthUser {
	
	private User user;
	private UserTokenState tokenState;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserTokenState getTokenState() {
		return tokenState;
	}

	public void setTokenState(UserTokenState tokenState) {
		this.tokenState = tokenState;
	}
	
	public AuthUser(User u, UserTokenState t) {
		this.user = u;
		this.tokenState = t;
	}
}
