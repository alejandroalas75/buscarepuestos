package io.javabrains.repuestos.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.User;
import io.javabrains.repuestos.repocitory.UserRepository;
import io.javabrains.repuestos.services.UserService;

import java.security.Principal;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
@RequestMapping( value = "/api", produces = MediaType.APPLICATION_JSON_VALUE )
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userrepocitory;

    @RequestMapping( method = GET, value = "/whoami/{userId}" )
    @PreAuthorize("hasRole('ADMIN')")
    public User loadById( @PathVariable Long userId ) {
        return this.userService.findById( userId );
    }

    @RequestMapping( method = GET, value= "/user/all")
    @PreAuthorize("hasRole('ADMIN')")//Tipo de rol que se requerira
    public List<User> loadAll() {
        return this.userService.findAll();
    }
    
    @RequestMapping(method = RequestMethod.POST , value = "/user/new")
    @PreAuthorize("hasRole('ADMIN')")
	public void addUser(@RequestBody User user) {
		userrepocitory.save(user);
		}
    
    @RequestMapping(method = RequestMethod.PUT , value = "/user/{id}")
    @PreAuthorize("hasRole('ADMIN')")
	public void updateUser(@RequestBody User user, Long id) {
		userrepocitory.save(user);
		}



    /*
     *  We are not using userService.findByUsername here(we could),
     *  so it is good that we are making sure that the user has role "ROLE_USER"
     *  to access this endpoint.
     */
    @RequestMapping("/whoami")
    @PreAuthorize("hasRole('USER')")
    public User user(Principal user) {
        return this.userService.findByUsername(user.getName());
    }
    
    @RequestMapping("/whoami/a")
    @PreAuthorize("hasRole('ADMIN')")
    public User user2(Principal user) {
        return this.userService.findByUsername(user.getName());
    }
}
