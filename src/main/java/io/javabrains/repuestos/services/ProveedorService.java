package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.repuestos.models.Proveedor;
import io.javabrains.repuestos.repocitory.ProveedorRepo;

@Service
public class ProveedorService {
	
	@Autowired
	ProveedorRepo proRep;
	
	public List<Proveedor> getAllProveedor(){
		List<Proveedor> proveedores =new ArrayList<>();
		proRep.findAll()
		.forEach(proveedores::add);
		return proveedores;	
	}
	
	
	public List<Proveedor> getProveedorByEstado(Boolean estado){
		List<Proveedor> proveedores =new ArrayList<>();
		proRep.findByEstado(estado)
		.forEach(proveedores::add);
		return proveedores;	
	}
	
	public Proveedor getProveedor(Integer id) {
		return proRep.findOne(id);
	}
	
	public void addProveedor(Proveedor proveedor) {
		proRep.save(proveedor);
	}
	
	public void updateProveedor(Proveedor proveedor, Integer id) {
		proRep.save(proveedor);
	}
	
	public void deleteProveedor(Integer id) {
		proRep.delete(id);
	}
	

}
