package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.repuestos.models.Municipio;
import io.javabrains.repuestos.repocitory.MunicipioRepo;

@Service
public class MunicipioService {
	
	@Autowired
	MunicipioRepo munRep;
	
	public List<Municipio> getByDepartamento(Integer codigoDep) {
		List<Municipio> municipios = new ArrayList<>();
		munRep.findByDepartamentoDepCodigo(codigoDep).forEach(municipios::add);
		return municipios;
		
	}

}
