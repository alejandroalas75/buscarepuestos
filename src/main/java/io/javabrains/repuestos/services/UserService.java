package io.javabrains.repuestos.services;

import io.javabrains.repuestos.models.User;

import java.util.List;


public interface UserService {
    User findById(Long id);
    User findByUsername(String username);
    List<User> findAll ();
    
    
    
}
