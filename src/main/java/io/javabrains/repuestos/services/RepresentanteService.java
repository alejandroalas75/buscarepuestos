package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.repuestos.models.Representante;
import io.javabrains.repuestos.repocitory.RepresentanteRepo;

@Service
public class RepresentanteService {
	
	@Autowired
	RepresentanteRepo repRepo;
	
	public List<Representante> getAllRepresentante(){
		List<Representante> reprsentantes =new ArrayList<>();
		repRepo.findAll()
		.forEach(reprsentantes::add);
		return reprsentantes;	
	}
	
	public List<Representante> getAllRepresentanteByEstado(Boolean estado){
		List<Representante> reprsentantes =new ArrayList<>();
		repRepo.findByEstado(estado)
		.forEach(reprsentantes::add);
		return reprsentantes;	
	}
	
	public Representante getRepresentante(Integer id) {
		return repRepo.findOne(id);
	}
	
	public void addRepresentante(Representante representante) {
		repRepo.save(representante);
	}
	
	public void updateRepresentante(Representante representante, Integer id) {
		repRepo.save(representante);
	}
	
	public void deleteReprsentante(Integer id) {
		repRepo.delete(id);
	}
	
}
