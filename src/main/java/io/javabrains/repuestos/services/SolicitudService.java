package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.javabrains.repuestos.models.Solicitud;
import io.javabrains.repuestos.repocitory.SolicitudRep;

@Service
public class SolicitudService {
	
	@Autowired
	SolicitudRep solRepocitory;
	
	public List<Solicitud> getAllSolicitud(){
		List<Solicitud> solicitudes =new ArrayList<>();
		solRepocitory.findAll()
		.forEach(solicitudes::add);
		return solicitudes;	
	}
	
	public void addSolicitud(Solicitud solicitud) {
		solRepocitory.save(solicitud);
	}
	
	public void updateSolicitud(Solicitud solicitud, Integer id) {
		solRepocitory.save(solicitud);
	}
	
	public void deleteSolicitud(Integer id) {
		solRepocitory.delete(id);
	}
	
	public List<Solicitud> getByestado(Integer estado){
		List<Solicitud> solicitudes = new ArrayList<>();
		solRepocitory.findByEstadoId(estado).forEach(solicitudes::add);
		return solicitudes;
	}

}
