package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.repuestos.models.Comision;
import io.javabrains.repuestos.repocitory.ComisionRepo;


@Service
public class ComisionService {

	
	@Autowired
	ComisionRepo comRepocitory;
	
	public List<Comision> getAllComision(){
		List<Comision> comisiones =new ArrayList<>();
		comRepocitory.findAll()
		.forEach(comisiones::add);
		return comisiones;	
	}
	
	public List<Comision> getComisionByEstado(Boolean estado){
		List<Comision> comisiones =new ArrayList<>();
		comRepocitory.findByEstado(estado)
		.forEach(comisiones::add);
		return comisiones;	
	}
	
	public Comision getComision(Integer Id) {
		return comRepocitory.findOne(Id);
	} 
	
	
	public void addComision(Comision comision) {
		comRepocitory.save(comision);
	}
	
	public void updateComision(Comision comision, Integer id) {
		comRepocitory.save(comision);
	}
	
	public void deleteComision(Integer id) {
		comRepocitory.delete(id);
	}
	


}
