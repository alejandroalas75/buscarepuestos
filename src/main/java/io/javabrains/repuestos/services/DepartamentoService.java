package io.javabrains.repuestos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.repuestos.models.Departamento;
import io.javabrains.repuestos.repocitory.DepartamentoRepo;

@Service
public class DepartamentoService {
	
	@Autowired
	DepartamentoRepo depRepo;
	
	public List<Departamento> getAllDepartamento(){
		List<Departamento> departamentos =new ArrayList<>();
		depRepo.findAll()
		.forEach(departamentos::add);
		return departamentos;	
	}
	

}
