package io.javabrains.repuestos.models;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Proveedor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pro_id")
	private Integer id;
	
	@Column(name = "pro_nombre", length = 100)
	private String nombre;
	
	@Column(name = "pro_direccion", length = 100 )
	private String direcccion;
	
	@Column (name = "pro_nit", length = 14)
	private String nit;
	
	@Column(name = "pro_estado")
	private Boolean estado;
	
	@OneToOne
	private Comision comision;
	
	@OneToOne
	private Municipio municipio;
	
	@OneToOne(cascade = {CascadeType.ALL, CascadeType.REMOVE}, optional= false )//Permite crear los dos objetos al mismo tiempo
	private Representante representante;
	

	public Proveedor() {

	}

	
	
	

	public Proveedor(Integer id, String nombre, String direcccion, String nit, Boolean estado, Comision comision,
			Municipio municipio, Representante representante) {
		this.id = id;
		this.nombre = nombre;
		this.direcccion = direcccion;
		this.nit = nit;
		this.estado = estado;
		this.comision = comision;
		this.municipio = municipio;
		this.representante = representante;
	}





	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Comision getComision() {
		return comision;
	}

	public void setComision(Comision comision) {
		this.comision = comision;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDirecccion() {
		return direcccion;
	}

	public void setDirecccion(String direcccion) {
		this.direcccion = direcccion;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "Proveedor [id=" + id + ", nombre=" + nombre + ", direcccion=" + direcccion + ", nit=" + nit
				+ ", estado=" + estado + ", comision=" + comision + ", municipio=" + municipio + ", representante="
				+ representante + "]";
	}

	
	
	

}
