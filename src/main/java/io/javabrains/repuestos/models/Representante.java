package io.javabrains.repuestos.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Representante implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name = "rep_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "rep_nombre", length = 100)
	private String nombre;
	
	@Column(name = "rep_correo")
	private String correo;
	
	@Column(name = "rep_telefono", length = 9)
	private String telefono;
	
	@Column(name = "rep_estado")
	private Boolean estado;
	
	@Column(name = "rep_cargo")
	private String cargo;
	
	@OneToOne
	private User user;

	public Representante() {
	}
	

	public Representante(Integer id, String nombre, String correo, String telefono, Boolean estado, String cargo,
			User user) {
		this.id = id;
		this.nombre = nombre;
		this.correo = correo;
		this.telefono = telefono;
		this.estado = estado;
		this.cargo = cargo;
		this.user = user;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public User getUsuario() {
		return user;
	}

	public void setUsuario(User usuario) {
		this.user = usuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Representante [id=" + id + ", nombre=" + nombre + ", correo=" + correo + ", telefono=" + telefono
				+ ", estado=" + estado + ", cargo=" + cargo + ", user=" + user + "]";
	}

	
	

}
