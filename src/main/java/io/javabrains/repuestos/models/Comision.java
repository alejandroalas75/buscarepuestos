package io.javabrains.repuestos.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Comision implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "com_id")
	private Integer id;
	
	@Column(name = "com_descripcion", length = 100)
	private String descripcion;
	
	@Column(name = "com_monto")
	private Double monto;
	
	@Column(name = "com_estado")
	private Boolean estado;
	
	@Column(name = "com_nombre", length = 100)
	private String nombre;

	public Comision() {
	}

	

	public Comision(Integer id, String descripcion, Double monto, Boolean estado, String nombre) {
		this.id = id;
		this.descripcion = descripcion;
		this.monto = monto;
		this.estado = estado;
		this.nombre = nombre;
	}



	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "Comision [id=" + id + ", descripcion=" + descripcion + ", monto=" + monto + ", estado=" + estado
				+ ", nombre=" + nombre + "]";
	}

	
	
	
	

}
