package io.javabrains.repuestos.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;



@Entity
public class Solicitud implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "sol_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "sol_nombreEmpresa", length = 100)
	private String nombreEmpresa;
	
	
	@Column(name = "sol_nombreRepresentante", length = 100)
	private String nombreRepresentante; 
	
	@Column(name = "sol_cargo", length = 50)
	private String cargo;
	
	@Column(name = "sol_correo", length = 50)
	private String correo;
	
	@Column(name = "sol_telefono", length = 8)
	private String telefono;
	
	@OneToOne
	@JoinColumn(name = "estate")
	private Estado estado;

	public Solicitud() {
	}

	public Solicitud(Integer id, String nombreEmpresa, String nombreRepresentante, String cargo, String correo,
			String telefono, Estado estado) {
		this.id = id;
		this.nombreEmpresa = nombreEmpresa;
		this.nombreRepresentante = nombreRepresentante;
		this.cargo = cargo;
		this.correo = correo;
		this.telefono = telefono;
		this.estado = estado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getNombreRepresentante() {
		return nombreRepresentante;
	}

	public void setNombreRepresentante(String nombreRepresentante) {
		this.nombreRepresentante = nombreRepresentante;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Solicitud [id=" + id + ", nombreEmpresa=" + nombreEmpresa + ", nombreRepresentante="
				+ nombreRepresentante + ", cargo=" + cargo + ", correo=" + correo + ", telefono=" + telefono
				+ ", estado=" + estado + "]";
	}
	
}
