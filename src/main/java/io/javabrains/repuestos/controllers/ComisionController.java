package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.Comision;
import io.javabrains.repuestos.services.ComisionService;

@RestController
public class ComisionController {
	
	@Autowired
	ComisionService comService;
	
	@RequestMapping("/comisiones")
	@PreAuthorize("hasRole('ADMIN')")
	public List<Comision> getAllComisiones(){
		return comService.getAllComision();
	}
	
	@RequestMapping(value="/comisiones/est/{estado}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public List<Comision> getAllComisionByEstado(@PathVariable Boolean estado){
		return comService.getComisionByEstado(estado);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/comisiones/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Comision getComision(@PathVariable Integer id) {
		return comService.getComision(id);
	}
	
	
	@RequestMapping(method = RequestMethod.POST , value = "/comisiones")
	@PreAuthorize("hasRole('ADMIN')")
	public void addComision(@RequestBody Comision comision) {
		comService.addComision(comision);	
	}
	@RequestMapping(method = RequestMethod.PUT, value="/comisiones/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void updateComision(@PathVariable Integer id,  @RequestBody Comision comision) {
		comService.updateComision(comision, id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value ="/comisiones/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteComision(@PathVariable Integer id) {
		comService.deleteComision(id);
	}

}
