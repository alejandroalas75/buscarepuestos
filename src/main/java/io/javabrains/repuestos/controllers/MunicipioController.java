package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.Municipio;
import io.javabrains.repuestos.services.MunicipioService;

@RestController
public class MunicipioController {
	
	@Autowired
	MunicipioService munService;
	
	@RequestMapping("/municipios/dep/{codigoDep}")
	public List<Municipio> getByEstado(@PathVariable Integer codigoDep){
		return munService.getByDepartamento(codigoDep);
	}
	

}
