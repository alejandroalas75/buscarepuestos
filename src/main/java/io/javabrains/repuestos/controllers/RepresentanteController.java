package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.Representante;
import io.javabrains.repuestos.services.RepresentanteService;

@RestController
public class RepresentanteController {
	
	@Autowired
	RepresentanteService repService;

	
	@RequestMapping("/representantes")
	@PreAuthorize("hasRole('ADMIN')")
	public List<Representante> getAllRepresentante(){
		return repService.getAllRepresentante();
	}
	
	@RequestMapping("/representantes/est/{estado}")
	@PreAuthorize("hasRole('ADMIN')")
	public List<Representante> getRepresentantesByestado(@PathVariable Boolean estado){
		return repService.getAllRepresentanteByEstado(estado);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/representantes")
	@PreAuthorize("hasRole('ADMIN')")
	public void addRepresentante(@RequestBody Representante representante) {
		repService.addRepresentante(representante);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/representantes/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Representante getRepresentante(@PathVariable Integer id) {
		return repService.getRepresentante(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/representantes/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void updateSolicitud(@PathVariable Integer id,  @RequestBody Representante representante) {
		repService.updateRepresentante(representante, id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value ="/representante/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteSolicitud(@PathVariable Integer id) {
		repService.deleteReprsentante(id);
	}
}
