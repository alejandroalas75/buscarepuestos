package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.Proveedor;
import io.javabrains.repuestos.services.ProveedorService;


@RestController
public class ProveedorController {
	
	@Autowired
	ProveedorService proService;
	
	@RequestMapping(value="/proveedores", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public List<Proveedor> getAllProveedor(){
		return proService.getAllProveedor();
		
	}
	
	@RequestMapping("/proveedores/est/{estado}")
	@PreAuthorize("hasRole('ADMIN')")
	public List<Proveedor> getAllProveedoresByEstado(@PathVariable Boolean estado){
		return proService.getProveedorByEstado(estado);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/proveedores", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public void addProveedor(@RequestBody Proveedor proveedor) {
		proService.addProveedor(proveedor);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value="/proveedores/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public Proveedor findById(@PathVariable Integer id) {
		return proService.getProveedor(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/proveedores/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void updateSolicitud(@PathVariable Integer id,  @RequestBody Proveedor proveedor) {
		proService.updateProveedor(proveedor, id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value ="/proveedores/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteSolicitud(@PathVariable Integer id) {
		proService.deleteProveedor(id);
	}

}
