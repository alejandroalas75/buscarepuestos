package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.javabrains.repuestos.models.Solicitud;
import io.javabrains.repuestos.services.SolicitudService;

@RestController
public class SolicitudController {
	
	@Autowired
	SolicitudService solService;
	
	
	@RequestMapping("/solicitudes")
    @PreAuthorize("hasRole('ADMIN')")
	public List<Solicitud> getAllSolicitudes(){
		return solService.getAllSolicitud();
	}
	
	@RequestMapping("/solicitudes/est/{estado}")
    @PreAuthorize("hasRole('ADMIN')")
	public List<Solicitud> getByEstado(@PathVariable Integer estado){
		return solService.getByestado(estado);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/solicitudes/add")
	public void addSolicitud(@RequestBody Solicitud solicitud) {
		solService.addSolicitud(solicitud);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/solicitudes/{id}")
    @PreAuthorize("hasRole('ADMIN')")
	public void updateSolicitud(@PathVariable Integer id,  @RequestBody Solicitud solicitud) {
		solService.updateSolicitud(solicitud, id);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value ="/solicitudes/{id}")
    @PreAuthorize("hasRole('ADMIN')")
	public void deleteSolicitud(@PathVariable Integer id) {
		solService.deleteSolicitud(id);
	}


}
