package io.javabrains.repuestos.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.repuestos.models.Departamento;
import io.javabrains.repuestos.services.DepartamentoService;

@RestController
public class DepartamentoController {
	
	@Autowired
	DepartamentoService depService;
	
	@RequestMapping(value="/departamentos")
	public List<Departamento> getAllDepartamentos(){
		return depService.getAllDepartamento();
	}
}
