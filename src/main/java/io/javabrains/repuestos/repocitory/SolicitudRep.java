package io.javabrains.repuestos.repocitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Solicitud;

@Repository
public interface SolicitudRep extends CrudRepository<Solicitud, Integer>{
	
	
	public List<Solicitud> findByEstadoId(Integer estate);
	

}
