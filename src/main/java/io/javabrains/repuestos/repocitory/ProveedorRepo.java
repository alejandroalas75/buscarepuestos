package io.javabrains.repuestos.repocitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Proveedor;

@Repository
public interface ProveedorRepo extends CrudRepository<Proveedor, Integer>{
	
	public List<Proveedor> findByEstado(Boolean estado);

}
