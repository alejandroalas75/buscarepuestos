package io.javabrains.repuestos.repocitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Representante;

@Repository
public interface RepresentanteRepo extends CrudRepository<Representante, Integer>{
	
	public List<Representante> findByEstado(Boolean estado);

}
