package io.javabrains.repuestos.repocitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Municipio;

@Repository
public interface MunicipioRepo extends CrudRepository<Municipio, Integer> {
	
	public List<Municipio> findByDepartamentoDepCodigo(Integer codigoDep);

}
