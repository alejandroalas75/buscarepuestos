package io.javabrains.repuestos.repocitory;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Departamento;

@Repository
public interface DepartamentoRepo extends CrudRepository<Departamento, Integer>{

}
