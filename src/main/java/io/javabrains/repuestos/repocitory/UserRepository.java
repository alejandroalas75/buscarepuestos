package io.javabrains.repuestos.repocitory;


import org.springframework.data.jpa.repository.JpaRepository;

import io.javabrains.repuestos.models.User;


public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername( String username );
}

