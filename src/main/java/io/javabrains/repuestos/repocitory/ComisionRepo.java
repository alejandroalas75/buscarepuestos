package io.javabrains.repuestos.repocitory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.javabrains.repuestos.models.Comision;

@Repository
public interface ComisionRepo extends CrudRepository<Comision, Integer>{
	
	public List<Comision> findByEstado(Boolean estado);
}
